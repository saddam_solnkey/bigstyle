const AlertMsg = {
    error:{
        INTERNET_CONNECTION:'Please check your internet connection.',
        CODE_NOT_MATCH:'Verification code not valid.',
    },
    
}

export default AlertMsg