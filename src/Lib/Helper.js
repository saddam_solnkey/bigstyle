import * as React from 'react';
import Toast from 'react-native-root-toast';
import { Alert, Platform, Keyboard } from 'react-native';
import AsyncStorage from "@react-native-community/async-storage";
import DeviceInfo from 'react-native-device-info'
import Config from './Config';

export default class Helper extends React.Component {
    url = "";
    static mainApp;
    static toast;
    static user = {};
    static navigationRef;
    static Loader;
    static device_type = Platform.OS == 'android' ? 'ANDROID' : 'IOS';
    static device_token = 'SIMULATOR';
    static hasNotch = DeviceInfo.hasNotch();
    static deviceRealHeight = 0;
    static userId = 0;

    static getFormData(obj) {
        let formData = new FormData();
        for (let i in obj) {
            formData.append(i, obj[i]);
        }
        return formData;
    }

    constructor(props) {
        super(props)
    }

    static registerNavigator(ref) {
        Helper.navigationRef = ref;
    }

    static registerLoader(mainApp) {
        Helper.mainApp = mainApp;
    }

    static registerLoged(mainApp) {
        Helper.mainApp = mainApp;
    }

    static showLoader() {
        Keyboard.dismiss();
        Helper.mainApp.setState({ loader: true })
    }

    static hideLoader() {
        Helper.mainApp.setState({ loader: false })
    }

    static registerToast(toast) {
        Helper.toast = toast;
    }

    static showToast(msg) {
        if (msg) {
            Toast.show(msg, {
                duration: 2000,
                position: Toast.positions.BOTTOM,
                shadow: true,
                animation: true,
                hideOnPress: true,
                delay: 0,
            });
        }
    }

    static alert(alertMessage, cb) {
        Alert.alert(
            Config.app_name,
            alertMessage,
            [
                { text: 'OK', onPress: () => { if (cb) cb(true); console.log('OK Pressed') } },
            ],
            { cancelable: false }
        )
    }

    static alertTitle(title, alertMessage, cb) {
        Alert.alert(
            title,
            alertMessage,
            [
                { text: 'OK', onPress: () => { if (cb) cb(true); console.log('OK Pressed') } },
            ],
            { cancelable: false }
        )
    }

    static confirm(alertMessage, cb) {
        Alert.alert(
            Config.app_name,
            alertMessage,
            [
                { text: 'OK', onPress: () => { if (cb) cb(true); console.log('OK Pressed') } },
                { text: 'Cancel', onPress: () => { if (cb) cb(false); }, style: 'cancel' },
            ],
            { cancelable: false }
        )
    }

    static confirmPopUp(alertMessage, cb) {
        Alert.alert(
            Config.app_name,
            alertMessage,
            [
                { text: 'YES', onPress: () => { if (cb) cb(true); console.log('OK Pressed') } },
                { text: 'NO', onPress: () => { if (cb) cb(false); }, style: 'cancel' },
            ],
            { cancelable: false }
        )
    }

    static permissionConfirm(alertMessage, cb) {
        Alert.alert(
            Config.app_name,
            alertMessage,
            [
                { text: 'EXIT', onPress: () => { if (cb) cb(false); }, style: 'cancel' },
                { text: 'SETTINGS', onPress: () => { if (cb) cb(true); console.log('OK Pressed') } },
            ],
            { cancelable: false }
        )
    }

    static cameraAlert(alertMessage, Camera, Gallery, Cancel, cbCamera, cbGallery) {
        Alert.alert(
            Config.app_name,
            alertMessage,
            [
                { text: Camera, onPress: () => { if (cbCamera) cbCamera(true); console.log('OK Pressed') } },
                { text: Gallery, onPress: () => { if (cbGallery) cbGallery(true); console.log('OK Pressed') } },
                { text: Cancel, onPress: () => { if (cbCamera) cbCamera(false); }, style: 'cancel' },
            ],
            { cancelable: false }
        )
    }

    static async setData(key, val) {
        try {
            let tempval = JSON.stringify(val);
            await AsyncStorage.setItem(key, tempval);
        } catch (error) {
            console.error(error, "AsyncStorage")
        }
    }

    static async getData(key) {
        try {
            let value = await AsyncStorage.getItem(key);
            if (value) {
                let newvalue = JSON.parse(value);
                return newvalue;
            } else {
                return value;
            }

        } catch (error) {
            console.error(error, "AsyncStorage")
        }
    }

    static async makeRequest({ url, data, method = "POST", loader = true }) {
        let finalUrl = Config.baseurl + url;
        console.log(finalUrl, "finalUrl");
        let form;
        let methodnew;
        let token = await this.getData("token");

        console.log(token, "tokentoken")
        console.log(data, "form")
        let varheaders;
        if (method == "POSTUPLOAD") {
            methodnew = "POST";
            varheaders = {
                Accept: 'application/json',
                'Content-Type': 'multipart/form-data;',
                Authorization: 'Bearer ' + token
            }
            form = data;
        }

        else if (method == "POST") {
            methodnew = "POST";
            if (token) {
                varheaders = {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                    // "Authorization": 'Bearer ' + token
                }
            } else {
                varheaders = {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                }
            }
            form = data;
        }
        else if (method == "FORMDATA") {
            methodnew = "POST";
            if (token) {
                varheaders = {
                    Accept: 'application/json',
                    'Content-Type': 'multipart/form-data',
                    // "Authorization": 'Bearer ' + token
                }
            } else {
                varheaders = {
                    Accept: 'application/json',
                    'Content-Type': 'multipart/form-data',
                }
            }
            form = data;
        }
        else {
            console.log('yes get');
            methodnew = "GET";
            if (token) {
                varheaders = {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                    "Authorization": 'Bearer ' + token
                }
            } else {
                varheaders = {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                }
            }
        }

        return fetch(finalUrl, {
            body: form,
            method: methodnew,
            headers: varheaders,
        })
            .then((response) => {
                return response.json()
            })
            .then((responseJson) => {
                console.log(responseJson, "responseJson")
                if (responseJson.hasOwnProperty('status_code')) {
                    if (responseJson.status_code === 401) {
                        AsyncStorage.removeItem('userdata');
                        AsyncStorage.removeItem('token');
                        this.showToast(responseJson.error);
                    }
                } else
                    return responseJson;
            })
            .catch((error, a) => {
                // this.showToast("Please check your internet connection.");
                console.log('errorerror', error);
            });
    }

    static getImageUrl(url) {
        return finalUrl = Config.imageUrl + '/' + url;
    }

    static checkNull(txt) {
        if (txt == null) {
            return ''
        } else {
            return txt
        }
    }

}


