import React, {Component} from 'react';
import {
  Text,
  StyleSheet,
  View,
  TextInput,
  TouchableOpacity,
  Image,
  Dimensions,
  Modal,
  Keyboard,
  PermissionsAndroid,
} from 'react-native';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import images from '../asstes/ImagePath';
import NetInfo from '@react-native-community/netinfo';
import {validators} from '../Lib/validationFunctions';
import AlertMsg from '../Lib/AlertMsg';
import Helper from '../Lib/Helper';
import ApiUrl from '../Lib/ApiUrl';
import Config from '../Lib/Config';
import Contacts from 'react-native-contacts';
import {openSettings} from 'react-native-permissions';

const ScreenHeight = Dimensions.get('screen').height;

export default class VerifyOtp extends Component {
  constructor(props) {
    super(props);
    this.state = {
    //   modalVisible: false,
      number: this.props.route.params.number,
      otp: '',
      userId: '',
    };
  }

  getContect = id => {
    PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.READ_CONTACTS, {
      title: 'Contacts',
      message: 'This app would like to view your contacts.',
      buttonPositive: 'Please accept bare mortal',
    })
      .then(data => {
        // console.log('=========>', data);
        if (data == 'granted') {
          Contacts.getAll()
            .then(rescon => {
              // console.log('--------------->contacData', rescon);
              let arr = [];
              rescon.length > 0 &&
                rescon.map(contactItem => {
                  let tempData = {
                    contactname: contactItem.displayName,
                    phonenumber: contactItem.phoneNumbers[0]?.number,
                    userid: id,
                  };
                  arr.push(tempData);
                  console.log(
                    '+++++++++++++++++++++++>arrarrarr' + JSON.stringify(arr),
                  );
                });
              setTimeout(() => {
                this.apiContacts(arr, id);
              }, 3000);
            })
            .catch(err => {
              console.log('err con : ', err);
            });
        }
        if (data == 'denied') {
          Helper.permissionConfirm('Jamil.', status => {
            if (status) {
              openSettings().catch(() => {
                console.warn('cannot open settings');
              });
            }
          });
          return;
        }
      })
      .catch(err => {
        console.log('=======>err', err);
      });
  };

  apiContacts = (arr, id) => {
    NetInfo.fetch().then(state => {
      if (!state.isConnected) {
        Helper.showToast(AlertMsg.error.INTERNET_CONNECTION);
        return false;
      } else {
        let tempPrems = {contactsData: JSON.stringify(arr)};
        var details = {
          json: JSON.stringify(tempPrems),
          userid: id,
        };
        console.log('=======>C data :  ', JSON.stringify(tempPrems));
        var formBody = [];
        for (var property in details) {
          var encodedKey = encodeURIComponent(property);
          var encodedValue = encodeURIComponent(details[property]);
          formBody.push(encodedKey + '=' + encodedValue);
        }
        formBody = formBody.join('&');
        fetch(Config.baseurl + ApiUrl.Contacts, {
          method: 'POST',
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8',
          },
          body: formBody,
        })
          .then(response => {
            return response.json();
          })
          .then(responseJson => {
            console.log(responseJson, 'responseJson');
            this.props.navigation.reset({
              index: 0,
              routes: [{name: 'MyWeb'}],
            });
          })
          .catch((error, a) => {
            console.log('errorerror', error);
          });

        return;
        let formdata = new FormData();
        formdata.append('contactsData', arr);
        formdata.append('userid', id);
        // var data = {
        //     contactsData: arr,
        //     userid: id,
        // };
        Helper.makeRequest({
          url: ApiUrl.Contacts,
          method: 'FORMDATA',
          data: formdata,
        })
          .then(response => {
            console.log(
              '=================>containtApi',
              JSON.stringify(response),
            );
            if (response.status == true) {
            }
          })
          .catch(err => {});
      }
    });
  };

  sendOtp() {
    Keyboard.dismiss();
    NetInfo.fetch().then(state => {
      if (!state.isConnected) {
        Helper.showToast(AlertMsg.error.INTERNET_CONNECTION);
        return false;
      } else {
        let formdata = new FormData();
        formdata.append('mobileno', this.state.number);
        formdata.append('otpnumber', this.state.otp);
        // var data = {
        //     mobileno: this.state.number,
        //     otpnumber: this.state.otp,
        // }
        // Helper.showLoader()
        Helper.makeRequest({
          url: ApiUrl.SEND_OTP,
          method: 'FORMDATA',
          data: formdata,
        })
          .then(response => {
            console.log('==========>Login' + JSON.stringify(response));
            if (response.status == true) {
              Helper.setData('userdata', response.data);
            //   this.setState({modalVisible: false});
              this.getContect(response.data.id);
              // Helper.hideLoader()
            } else {
              // Helper.hideLoader()
              Helper.showToast(response.message);
            }
          })
          .catch(err => {});
      }
    });
  }

  resendApi() {
    NetInfo.fetch().then(state => {
      if (!state.isConnected) {
        alert('if');
        Helper.showToast(AlertMsg.error.INTERNET_CONNECTION);
        return false;
      } else {
        if (
          validators.checkPhoneNumber(
            'Mobile Number',
            5,
            15,
            this.state.number.trim(),
          )
        ) {
          let formdata = new FormData();
          formdata.append('phonenunmbetr', this.state.number.trim());
          // var data = {
          //     phonenunmbetr: this.state.number.trim(),
          // }
          Helper.showLoader();
          Helper.makeRequest({
            url: ApiUrl.LOGIN,
            method: 'FORMDATA',
            data: formdata,
          })
            .then(response => {
              console.log('==========>OTP<======' + JSON.stringify(response));
              if (response.status == true) {
                  alert("------>otp<----" + response.data.otp)
                Helper.hideLoader();
              } else {
                Helper.hideLoader();
                Helper.showToast(response.message);
              }
            })
            .catch(err => {});
        }
      }
    });
  }

  render() {
    return (
      <View style={styles.containerView}>
        <KeyboardAwareScrollView>
          <View style={styles.imgmainView}>
            <Image
              source={images.bigStyleLogo}
              style={{height: 200, width: 200, resizeMode: 'contain'}}
            />
          </View>
            <View style={styles.mainView}>
                <Text style={[styles.labelTextCss, {marginTop: 10}]}>
                    Your Mobile OTP
                </Text>
                <View style={styles.viewOffInput}>
                    <TextInput
                    style={{
                        flex: 1,
                        color: 'grey',
                        fontSize: 16,
                        letterSpacing: 5,
                        textAlign: 'center',
                    }}
                    placeholder={'Enter OTP'}
                    placeholderTextColor={'grey'}
                    onChangeText={text => {
                        this.setState({otp: text.replace(/[^0-9]/g, '')});
                    }}
                    value={this.state.otp}
                    returnKeyType={'done'}
                    keyboardType={'number-pad'}
                    maxLength={6}
                    />
                </View>
                <TouchableOpacity
                    onPress={() => {
                    this.sendOtp();
                    }}
                    activeOpacity={0.8}
                    style={styles.bttViewCss}>
                    <Text style={styles.bttTextCss}>Submit</Text>
                </TouchableOpacity>
                <TouchableOpacity
                    onPress={() => {this.resendApi()}}
                    style={{alignSelf: 'center'}}
                    hitSlop={{right: 30, left: 30, top: 10, bottom: 10}}>
                    <Text style={styles.resendTextCss}>Resend</Text>
                </TouchableOpacity>
            </View>
        </KeyboardAwareScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  containerView: {flex: 1},
  imgmainView: {
    backgroundColor: '#fff',
    height: ScreenHeight / 2.3,
    alignItems: 'center',
    justifyContent: 'center',
  },
  labelTextCss: {color: '#000', fontSize: 16, fontWeight: 'bold'},
  viewOffInput: {
    height: 45,
    paddingHorizontal: 10,
    borderWidth: 1,
    borderColor: 'grey',
    borderRadius: 6,
    marginTop: 20,
  },
  bttViewCss: {
    alignSelf: 'center',
    backgroundColor: '#5c8727',
    width: '80%',
    padding: 12,
    marginVertical: 30,
    borderRadius: 10,
  },
  bttTextCss: {
    color: '#fff',
    fontSize: 16,
    fontWeight: 'bold',
    textAlign: 'center',
  },
  mainView: {
    backgroundColor: '#F2F1F0',
    padding: 20,
    paddingTop: 30,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    marginTop: -20,
  },
  mainView: {
    backgroundColor: '#F2F1F0',
    padding: 20,
    paddingTop: 30,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    marginTop: -20,
  },
  resendTextCss: {color: '#146eb3', fontSize: 16, fontWeight: 'bold'},
});
