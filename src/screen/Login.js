import React, {Component} from 'react';
import {
  Text,
  StyleSheet,
  View,
  TextInput,
  TouchableOpacity,
  Image,
  Dimensions,
  Modal,
  Keyboard,
  PermissionsAndroid,
} from 'react-native';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import images from '../asstes/ImagePath';
import NetInfo from '@react-native-community/netinfo';
import {validators} from '../Lib/validationFunctions';
import AlertMsg from '../Lib/AlertMsg';
import Helper from '../Lib/Helper';
import ApiUrl from '../Lib/ApiUrl';
import Config from '../Lib/Config';
import Contacts from 'react-native-contacts';
import {openSettings} from 'react-native-permissions';

const ScreenHeight = Dimensions.get('screen').height;

export default class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      otpViewShow: false,
      number: '',
      otp: '',
      userId: '',
    };
  }

  mobileVaryfy() {
    NetInfo.fetch().then(state => {
      if (!state.isConnected) {
        alert('if');
        Helper.showToast(AlertMsg.error.INTERNET_CONNECTION);
        return false;
      } else {
        if (
          validators.checkPhoneNumber(
            'Mobile Number',
            5,
            15,
            this.state.number.trim(),
          )
        ) {
          let formdata = new FormData();
          formdata.append('phonenunmbetr', this.state.number.trim());
          // var data = {
          //     phonenunmbetr: this.state.number.trim(),
          // }
          Helper.showLoader();
          Helper.makeRequest({
            url: ApiUrl.LOGIN,
            method: 'FORMDATA',
            data: formdata,
          })
            .then(response => {
              console.log('==========>OTP<======' + JSON.stringify(response));
              if (response.status == true) {
                this.props.navigation.navigate("VerifyOtp",{number: this.state.number})
                // Helper.showToast(response.message)
                Helper.hideLoader();
              } else {
                Helper.hideLoader();
                Helper.showToast(response.message);
              }
            })
            .catch(err => {});
        }
      }
    });
  }

  render() {
    return (
      <View style={styles.containerView}>
        <KeyboardAwareScrollView>
          <View style={styles.imgmainView}>
            <Image
              source={images.bigStyleLogo}
              style={{height: 200, width: 200, resizeMode: 'contain'}}
            />
          </View>
          <View style={styles.mainView}>
            <Text style={styles.labelTextCss}>Mobile Number</Text>
            <View style={styles.viewOffInput}>
              <TextInput
                style={{flex: 1, color: 'grey', fontSize: 16}}
                placeholder={'Enter Mobile Number'}
                placeholderTextColor={'grey'}
                onChangeText={text => {
                  this.setState({number: text.replace(/[^0-9]/g, '')});
                }}
                value={this.state.number}
                returnKeyType={'done'}
                keyboardType={'number-pad'}
                maxLength={15}
              />
            </View>
            <TouchableOpacity
              onPress={() => {
                this.mobileVaryfy();
              }}
              activeOpacity={0.8}
              style={[styles.bttViewCss, {marginTop: 30}]}>
              <Text style={styles.bttTextCss}>Submit</Text>
            </TouchableOpacity>
          </View>
        </KeyboardAwareScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  containerView: {flex: 1},
  imgmainView: {
    backgroundColor: '#fff',
    height: ScreenHeight / 2.3,
    alignItems: 'center',
    justifyContent: 'center',
  },
  labelTextCss: {color: '#000', fontSize: 16, fontWeight: 'bold'},
  viewOffInput: {
    height: 45,
    paddingHorizontal: 10,
    borderWidth: 1,
    borderColor: 'grey',
    borderRadius: 6,
    marginTop: 8,
  },
  bttViewCss: {
    alignSelf: 'center',
    backgroundColor: '#5c8727',
    width: '80%',
    padding: 12,
    marginVertical: 20,
    borderRadius: 10,
  },
  bttTextCss: {
    color: '#fff',
    fontSize: 16,
    fontWeight: 'bold',
    textAlign: 'center',
  },
  mainView: {
    backgroundColor: '#F2F1F0',
    padding: 20,
    paddingTop: 30,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    marginTop: -20,
  },
});
