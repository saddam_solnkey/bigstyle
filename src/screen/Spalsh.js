import React, { Component } from 'react'
import { Text, StyleSheet, View } from 'react-native'
import Helper from '../Lib/Helper';
import SplashScreen from 'react-native-splash-screen';

export default class Spalsh extends Component {
    constructor(props) {
        super(props)
        this.state = {
            userId: "",
        }
    }

    componentDidMount() {
        setTimeout(() => {
            SplashScreen.hide();
        }, 1000);

        Helper.getData('userdata').then((userdata) => {
            if (userdata === null) {
                this.props.navigation.reset({
                    index: 0,
                    routes: [{ name: "Login" }],
                });
            } else {
                this.props.navigation.reset({
                    index: 0,
                    // routes: [{ name: "Login" }],
                    routes: [{ name: "MyWeb" }],
                });
            }
        })
    }

    render() {
        return (
            <View style={{ flex: 1 }} />
        )
    }
}