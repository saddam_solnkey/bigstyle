import React from "react";
import { View, ActivityIndicator, StyleSheet } from "react-native";
import { WebView } from 'react-native-webview';
import SplashScreen from 'react-native-splash-screen';
import Helper from './Lib/Helper';

export default class MyWeb extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            urlWebview:''
        }
        Helper.getData('userdata').then((userdata) => {
            let url="https://bigstyle.in/userphonelogin?phonenumber="+userdata.phonenum+"&phoneOtp="+userdata.userotp;
            console.log("full url --- > : "+url);
            this.setState({urlWebview:url});
        })
    }

    componentDidMount() {
        setTimeout(() => {
            SplashScreen.hide();
        }, 1000);
    }

    _renderLoading() {
        return (
            <View style={styles.container}>
                <ActivityIndicator
                    size={"large"}
                    color={"#000"} />
            </View>
        );
    }

    render() {
        return (
            <WebView
                style={styles.container}
                source={{ uri: this.state.urlWebview }}
                javaScriptEnabled={true}
                domStorageEnabled={true}
                renderLoading={this._renderLoading}
                startInLoadingState={true}
            />
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1, backgroundColor: "#fff"
    },
});