import React, { Component } from 'react';
import { View } from 'react-native';
import CustomLoader from './src/Lib/CustomLoader';
import Routes from './src/navigation/Routes';

export default class App extends Component {
  constructor(props) {
    super(props)
    this.state = {
    }
  }

  render() {
    return (
      <View style={{ flex: 1, elevation: 30, zIndex: 30 }}>
        <Routes />
        <CustomLoader />
      </View>
    )
  }
}